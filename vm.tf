resource "yandex_compute_instance" "front" {
  count = var.vm_count
  name  = "frontend-${count.index}"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = join("\n", [for key in var.ssh_keys : "${key.user}:${key.publickey}"])
  }
}

resource "yandex_compute_instance" "back" {
  count = var.vm_count
  name  = "backend-${count.index}"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = join("\n", [for key in var.ssh_keys : "${key.user}:${key.publickey}"])
  }
}


output "internal_ip_address_front" {
  value = yandex_compute_instance.front.*.network_interface.0.ip_address
}


output "external_ip_address_front" {
  value = yandex_compute_instance.front.*.network_interface.0.nat_ip_address
}

output "internal_ip_address_back" {
  value = yandex_compute_instance.back.*.network_interface.0.ip_address
}


output "external_ip_address_back" {
  value = yandex_compute_instance.back.*.network_interface.0.nat_ip_address
}
